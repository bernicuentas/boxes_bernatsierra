package boxes;

public class Edge {

    private int x, y;
    private boolean isH;

    public Edge(boolean isH, int x, int y) {
        this.x = x;
        this.y = y;
        this.isH = isH;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isIsH() {
        return isH;
    }

    @Override
    public String toString() {
        return "Edge{" + "x=" + x + ", y=" + y + ", isH=" + isH + '}';
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean eq=false;
        if (this == obj) {
            eq = true;
        } else if (obj instanceof Edge) {
            Edge ed = (Edge) obj;
            eq = this.x == ed.x && this.y == ed.getY() ;            
        }        
        return eq;
    }
}

package boxes;

import java.util.List;

public interface BoxesContract {

	interface BoxesView {
		
		void setPresenter(BoxesPresenter p);
		
		// other methods called by presenter here
		void drawBox(int player, List<Edge> edgeToPaint);
		void say(String text);
		void setScore(int player, int score);
		void setTurn(int player);
                void unPaint();
                void paintEdge(int player, boolean clicked);
	}
	
	interface BoxesPresenter extends BoxesModelListener {
		
		void setView(BoxesView v);		
		void setModel(BoxesModel m);
		
		// other methods called by view here
		
		void enterEvent(Edge e);
		void leaveEvent(Edge e);
		void clickEvent(Edge e);
	}
	
	interface BoxesGame {
		
		// methods called by presenter here
		
		int getSize();
		int getTurn();
                void setTurn();
		int getScore(int player);
		int getBox(int x, int y);
		int getEdge(Edge e);				
		boolean play(int player, Edge e);
                List<Edge> returnListBoxes();
	}
	
	interface BoxesModel extends BoxesGame {
            
	}
	
	interface BoxesModelListener {

		void edgeEvent(int player, Edge e);
		void overEvent();
		void turnEvent(int player);
	}
}

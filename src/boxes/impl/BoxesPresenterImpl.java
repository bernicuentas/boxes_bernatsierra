package boxes.impl;

import boxes.BoxesContract.BoxesModel;
import boxes.BoxesContract.BoxesPresenter;
import boxes.BoxesContract.BoxesView;
import boxes.Edge;
import java.util.ArrayList;
import java.util.List;

public class BoxesPresenterImpl implements BoxesPresenter {

	private BoxesView view;
	private BoxesModel model;
	
	public BoxesPresenterImpl() {
	}
	
	@Override
	public void setView(BoxesView v) {		
		this.view = v;
	}

	@Override
	public void setModel(BoxesModel m) {		
		this.model = m;
	}
		
	// EVENTS FROM VIEW

	@Override
	public void enterEvent(Edge e) {
            if (model.getEdge(e) == 0) {
                view.paintEdge(0, false);
            }
	}

	@Override
	public void leaveEvent(Edge e) {
            if (model.getEdge(e) == 0) {
                view.unPaint(); 
            }
	}

	@Override
	public void clickEvent(Edge e) {
            List<Edge> ed = new ArrayList<Edge>();
            if (model.getEdge(e) == 0) {
                if (model.play(model.getTurn(), e)) {
                    edgeEvent(model.getTurn(), e);
                    model.setTurn();
                    turnEvent(model.getTurn());
                } else {
                    edgeEvent(model.getTurn(), e);
                    ed = model.returnListBoxes();
                    view.drawBox(model.getTurn(), ed);
                    view.setScore(model.getTurn(), model.getScore(model.getTurn())); 
                    if (model.getScore(1) + model.getScore(2) == 16) {
                        overEvent();
                    } 
                }
            }
	}
	
	@Override
	public void edgeEvent(int player, Edge e) {
            view.paintEdge(player, true);
	}

	@Override
	public void overEvent() {
            view.say("GAME OVER!!!!!!!!!!!!!!");
	}

	@Override
	public void turnEvent(int player) {	
            view.setTurn(player);
	}
}

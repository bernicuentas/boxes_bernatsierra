package boxes.impl;

import javafx.stage.Stage;
import boxes.BoxesContract.BoxesPresenter;
import boxes.BoxesContract.BoxesView;
import boxes.Edge;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class BoxesViewImpl implements BoxesView {

    private BoxesPresenter p;

    private int size, LX, DX, LY, DY, M = 50, HEIGHT = 1000, WIDTH = 1000;
    private int lastX = M, lastY = M, lastW = 0, lastH = 0, lastEdgeX = 0, lastEdgeY = 0;
    int fila = 0, columna = 0, player = 0;
    boolean isH = false, lastEdgeIsH = false;
    Canvas canvas;
    GraphicsContext gc;

    public BoxesViewImpl(Stage primaryStage, int size) {
        p = new BoxesPresenterImpl();
        this.size = size;
        LX = WIDTH - 2 * M;
        DX = LX / (size - 1);
        LY = HEIGHT - 2 * M;
        DY = LY / (size - 1);
        initUI(primaryStage);
    }

    @Override
    public void setPresenter(BoxesPresenter p) {
        this.p = p;
    }

    /**
     * Dibuixa la caixa en cas de que un player hagi tancat un cuadrat
     * @param player
     * @param edgeToPaint 
     */
    @Override
    public void drawBox(int player, List<Edge> edgeToPaint) {
        int x, y, w, h;
        if (player == 1) {
            Paint p = Color.RED;
            gc.setFill(p);
        } else {
            Paint p = Color.BLUE;
            gc.setFill(p);
        }
        for (Edge edge : edgeToPaint) {
            x=0; y=0; w=213; h=213;
            x = edge.getX() - 5;
            y = edge.getY();
            x += (x * DX) + 62;
            y += (y * DY) + 62;
            gc.fillRect(x, y, w, h);
        }
    }

    /**
     * Metode utilitzat com a funcio final per a terminar el joc
     * @param text 
     */
    @Override
    public void say(String text) {
        Paint p = Color.WHITE;
        gc.setFill(p);
        gc.fillRect(463, 1000, 100, 20);
        p = Color.DARKGREEN;
        gc.setFill(p);
        gc.fillText(text, 463, 1010);
    }

    /**
     * Canvia el marcador en funció de qui hagi puntuat
     * @param player
     * @param score 
     */
    @Override
    public void setScore(int player, int score) {
        Paint p = Color.WHITE;
        gc.setFill(p);
        if (player  == 1) {
            gc.fillRect(200, 1000, 20, 20);
            p = Color.RED;
            gc.setFill(p);
            gc.fillText(Integer.toString(score), 200, 1010);
        }else{
            gc.fillRect(930, 1000, 20, 20);
            p = Color.BLUE;
            gc.setFill(p);
            gc.fillText(Integer.toString(score), 930, 1010);
        }
    }

    /**
     * Canvia el text que hi ha al motg del tauler indicant a quin player li toca
     * @param player 
     */
    @Override
    public void setTurn(int player) {
        Paint p = Color.WHITE;
        gc.setFill(p);
        gc.fillRect(463, 1000, 100, 20);
        
        if (player == 1) {
            p = Color.RED;
            gc.setFill(p); 
        }else{
            p = Color.BLUE;
            gc.setFill(p);
        }
        
        gc.fillText("Turn del player "+player, 463, 1010);
    }

    /**
     * Inicialitzem el UI amb els events de click i move
     * @param primaryStage 
     */
    // DRAW
    private void initUI(Stage primaryStage) {
        canvas = new Canvas(1000, 1050);
        int x, y = M;
        // Obtenir Graphics Contexts
        gc = canvas.getGraphicsContext2D();
        Paint punt = Color.BLACK;
        gc.setFill(punt);
        for (int i = 0; i < 5; i++) {
            x = M;
            for (int j = 0; j < 5; j++) {
                Paint p1 = Color.BLACK;
                gc.setFill(p1);
                gc.fillRect(x, y, 15, 15);
                x += DX;
            }
            y += DY;
        }

        // Afegir controlador(listener) de MouseEvent.MOUSE_CLICKED
        canvas.addEventHandler(MouseEvent.MOUSE_MOVED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                double x = e.getX();
                double y = e.getY();
                double w = M, h = M;
                if (mouseEvent(x, y, w, h)) {
                    lastEdgeX = columna;
                    lastEdgeY = fila;
                    lastEdgeIsH = isH;
                    Edge ed = new Edge(isH, columna, fila);
//                  System.out.println("Edge de mouseMoved true: "+ed);
                    p.enterEvent(ed);
                } else {
                    Edge ed = new Edge(lastEdgeIsH, lastEdgeX, lastEdgeY);
                    p.leaveEvent(ed);
                }
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                double x = e.getX();
                double y = e.getY();
                double w = M, h = M;
//                

                if (mouseEvent(x, y, w, h)) {
                    Edge ed = new Edge(isH, columna, fila);
                    System.out.println(ed);
                    p.clickEvent(ed);
                }

//                unPaint(lastX, lastY, lastW, lastH);
            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(canvas);
        Scene scene = new Scene(root, 1000, 1050);
        gc.fillText("Marcador del vermell: ", 50, 1010);
        gc.fillText("0", 200, 1010);
        gc.fillText("Marcador del blau: ", 800, 1010);
        gc.fillText("0", 930, 1010);
        Paint p = Color.RED;
        gc.setFill(p);
        gc.fillText("Turn del player 1", 463, 1010);
        primaryStage.setTitle("Boxes de Bernat Sierra");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Metode per a pintar una aresta, segons si s'ha clicat, si només és un
     * hover, i en cas de ser clicat quin player ho ha fet.
     * @param player
     * @param clicked 
     */
    @Override
    public void paintEdge(int player, boolean clicked) {
        int x = M, y = M, w = 0, h = 0;
        if (isH) {
            columna -= 5;
            x += (columna * DX) + 15;
            y += fila * DY;
            w = 210;
            h = 15;
        } else {
            fila -= 5;
            x += columna * DX;
            y += (fila * DY) + 15;
            w = 15;
            h = 210;
        }

        if (!clicked) {
            Paint p = Color.GREY;
            gc.setFill(p);
            gc.fillRect(x, y, w, h);
            lastX = x; lastY = y; lastW = w; lastH = h;
        } else {
            if (player == 1) {
                Paint p = Color.RED;
                gc.setFill(p);
                gc.fillRect(x, y, w, h);
            } else {
                Paint p = Color.BLUE;
                gc.setFill(p);
                gc.fillRect(x, y, w, h);
            }
            
        }
    }

    /**
     * Funció per borrar l'última aresta registrda
     */
    @Override
    public void unPaint() {
        Paint p = Color.WHITE;
        gc.setFill(p);
        gc.fillRect(lastX, lastY, lastW, lastH);
    }

    /**
     * Capta el moviment del mouse, transformant les coordenades en Integer sencills,
     * si està tocant un punt en compter de ser una aresta torna la posició normal,
     * del 1 al 4, en canvi si esta entre mitg de dos punts torna el valor del 1
     * al 4 més 5. Per exemple, si volem dibuixar una aresta Horitzontal entre mitg
     * de la x 0 i 1, i la y amb valor 0, la x valdrà 5 (0 + 5) i la y valdrà 0 (5,0).
     * Torna true si és una aresta.
     * @param x
     * @param y
     * @param w
     * @param h
     * @return 
     */
    private boolean mouseEvent(double x, double y, double w, double h) {
        boolean findX = false, edge = false;
        for (int i = 0; i < 5; i++) {
            w = M;
            for (int j = 0; j < 5; j++) {
                if (x >= w && x <= w + 15 && y >= M) {
                    columna = j;
                    findX = true;
                    break;
                } else if (x >= w + 15 && x <= w + DX && y >= M) {
                    columna = j + 5;
                }
                w += DX;
            }
            if (y >= h && y <= h + 15 && x >= M) {
                fila = i;
                if (findX) {
                    break;
                }
            } else if (y >= h + 15 && y <= h + DY && x >= M) {
                fila = i + 5;
            }
            h += DY;
        }
//        paint(columna, fila, clicked);
//
        System.out.println("X: " + columna + " Y: " + fila);
        System.out.println((int)x + " " + (int)y);

        if (columna >= 5 && columna < 9 && fila < 5 && fila >= 0) {
            isH = true;
            edge = true;
        } else if (columna < 5 && columna >= 0 && fila >= 5 && fila < 9) {
            isH = false;
            edge = true;
        }
        
        return edge;
    }
}

package boxes.impl;

import boxes.BoxesContract.BoxesModel;
import boxes.BoxesContract.BoxesModelListener;
import boxes.BoxesContract.BoxesPresenter;
import boxes.Edge;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoxesModelImpl implements BoxesModel {

    private int size;
    private int turn; // 0=off, 1=player1, 2=player2
    private int hori[][], vert[][], fill[][];
    private int redCounter = 0, blueCounter = 0;
    boolean changePlayer;
    private Map<Edge, Integer> listeners;
    private List<Edge> edgeToPaint;

    public BoxesModelImpl(int size) {
        this.size = size;
        this.turn = 1;
        this.listeners = new HashMap<Edge, Integer>();
        this.edgeToPaint = new ArrayList<Edge>();
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getTurn() {
        return turn;
    }

    @Override
    public void setTurn() {
        if (turn == 1) {
            turn = 2;
        } else {
            turn = 1;
        }
    }

    @Override
    public int getScore(int player) {
        return player == 1 ? redCounter : blueCounter;
    }

    @Override
    public int getBox(int x, int y) {
        return 0;
    }

    @Override
    public int getEdge(Edge e) {
        int player = 0;
        for (Map.Entry<Edge, Integer> entry : listeners.entrySet()) {
            if (entry.getKey().getX() == e.getX() && entry.getKey().getY() == e.getY()) {
                player = entry.getValue();
            }
        }
//        for (Map.Entry<Edge, Integer> entry : listeners.entrySet()) {
//            System.out.println(entry);
//        }

        return player;
    }

    @Override
    public boolean play(int player, Edge e) {
        int x = e.getX(), y = e.getY();
        boolean isH = e.isIsH();
        changePlayer = true;
        cleanListBoxes();
        //afegim l'aresta al listener
        listeners.put(e, player);
        //comprovem que si la box esta completa
        if (isH) {
            if (!(y - 1 < 0)) {
                Edge e1 = new Edge(false, x - 5, y + 4);
                Edge e2 = new Edge(true, x, y - 1);
                Edge e3 = new Edge(false, x - 4, y + 4);
                if (boxesCalc(e1, e2, e3, player)) {
                    edgeToPaint.add(e2);
                }
            }
            if (!(y + 1 > 4)) {
                Edge e5 = new Edge(true, x, y + 1);
                Edge e4 = new Edge(false, x - 5, y + 5);
                Edge e6 = new Edge(false, x - 4, y + 5);
                if (boxesCalc(e4, e5, e6, player)) {
                    edgeToPaint.add(e);
                }
            }
        } else {
            if (!(x - 1 < 0)) {
                Edge e1 = new Edge(false, x - 1, y);
                Edge e2 = new Edge(true, x + 4, y - 5);
                Edge e3 = new Edge(true, x + 4, y - 4);
                if (boxesCalc(e1, e2, e3, player)) {
                    edgeToPaint.add(e2);
                }
            }
            if (!(x + 1 > 4)) {
                Edge e5 = new Edge(false, x + 1, y);
                Edge e4 = new Edge(true, x + 5, y - 5);
                Edge e6 = new Edge(true, x + 5, y - 4);
                if (boxesCalc(e4, e5, e6, player)) {
                    edgeToPaint.add(e4);
                }
                
            }
        }

//        System.out.println("ChangePlayer: "+changePlayer);
//        for (Map.Entry<Edge, Integer> entry : listeners.entrySet()) {
//            System.out.println(entry);
//        }
        return changePlayer;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < size - 1; y++) {
            for (int x = 0; x < size - 1; x++) {
                sb.append(" ");
                sb.append(hori[x][y] != 0 ? "-" : " ");
            }
            sb.append(" \n");
            for (int x = 0; x < size - 1; x++) {
                sb.append(vert[x][y] != 0 ? "|" : " ");
                sb.append(fill[x][y] != 0 ? Integer.toString(fill[x][y]) : " ");
            }
            sb.append(vert[size - 1][y] != 0 ? "|" : " ");
            sb.append("\n");
        }
        for (int x = 0; x < size - 1; x++) {
            sb.append(" ");
            sb.append(hori[x][size - 1] != 0 ? "-" : " ");
        }
        sb.append(" \n");

        return sb.toString();
    }

    private boolean boxesCalc(Edge e1, Edge e2, Edge e3, int player) {
        boolean change = false;
        int counter = 0;
        for (Map.Entry<Edge, Integer> entry : listeners.entrySet()) {
            if (entry.getKey().equals(e1) || entry.getKey().equals(e2) || entry.getKey().equals(e3)) {
                counter++;
            }
        }
        
        if (counter == 3) {
            changePlayer = false;
            change = true;
            if (player == 1) {
                redCounter++;
            }else{
                blueCounter++;
            }
        }
//        if (listeners.containsKey(e1) && listeners.containsKey(e2) && listeners.containsKey(e3)) {
//            System.out.println("hola 5");
//            if (player == 1) {
//                redCounter++;
//            } else {
//                blueCounter++;
//            }
//            changePlayer = false;
//        }   

        return change;
    }
    
    public List<Edge> returnListBoxes(){
        return edgeToPaint;
    }
    
    private void cleanListBoxes(){
        edgeToPaint.clear();
    }

}
